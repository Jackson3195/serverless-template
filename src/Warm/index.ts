import { Context } from '@azure/functions';

module.exports = async function (ctx: Context) {
  ctx.log.info('Warming API - ' + new Date().toISOString());
  // Environment variables check
  if (!process.env.SENDGRID_API_KEY) {
    ctx.log.error('Environment variable not set SENDGRID_API_KEY');
  }
  // Send response
  ctx.res.body = {
    status: 'Warmed',
    timestamp: new Date(),
  };
  ctx.res.status = 200;
};
