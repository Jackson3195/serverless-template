import * as uuidv4 from 'uuid/v4';
import * as bcrypt from 'bcrypt';
import { NetworkError } from '@lordly/interfaces/azure';

export function newUUID (length: number) {
  // Strip dashes
  let strippedUUID: string = String(uuidv4()).replace(/-/g, '');
  // Split old char array
  const oldUUIDCharArray: string [] = strippedUUID.split('');
  // Create new char array
  let newUUIDCharArray: string[] = [];
  // Generate random number
  for (let i: number = 0; i < length; i++) {
    const rdnNbr: number = Math.floor(Math.random() * (strippedUUID.length - 0) + 0);
    newUUIDCharArray.push(oldUUIDCharArray[rdnNbr]);
  }
  return newUUIDCharArray.join('');
}

export async function createHash (pwd: string, granules: string, errors: NetworkError[]) {
  const temppass: string = pwd + granules;
  let hashedPassword: string;
  await bcrypt.hash(temppass, 13).then((hash: string) => {
    hashedPassword = hash;
  }).catch((err) => {
    errors.push({message: String(err)});
  });
  return hashedPassword;
}

export function compareHash (pwd: string, granules: string, hash: string) {
  const temppass: string = pwd + granules;
  return bcrypt.compareSync(temppass, hash);
}
