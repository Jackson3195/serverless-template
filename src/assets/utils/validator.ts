// Validator
export class Validator {
  // Class variables
  // Constructor
  // Methods
  public Escape (value: string): string | undefined {
    if (value) {
      return String(value).replace(/(<|\?|>|;|-{2,}|\'|)/gm, '');
    } else {
      return undefined;
    }
  }
  public HardEscape (value: string): string {
    return String(value).replace(/(<|\?|>|;|-{2,}|\'|\"|:|#|$|&{2,}|!{2,})/gm, '');
  }
}
